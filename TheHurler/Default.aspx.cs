﻿using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;
using TheHurler.Helper;

namespace TheHurler
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindBlogs();
            }
        }

        private void BindBlogs()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            List<BlogMasterModel> blogs = (from b in _ctx.BlogMasters
                                           select new BlogMasterModel
                                           {
                                               BlogId = b.BlogId,
                                               Title = b.Title,
                                               Tag = b.Tag,
                                               Author = b.Author,
                                               ImageUrl = b.ImageUrl,
                                               PublishDate = b.PublishDate,
                                               BlogBody = b.BlogBody
                                           }).OrderByDescending(x => x.PublishDate).Take(3).ToList();

            if (blogs != null && blogs.Any())
            {
                blogs.ForEach(x =>
                {
                    x.Title = (x.Title.Length > 20 ? x.Title.Substring(0, 19) : x.Title);
                    x.BlogBody = Utilities.StripHtml(x.BlogBody);
                });
                rptBlogs.DataSource = blogs;
            }
            else
                rptBlogs.DataSource = null;
            rptBlogs.DataBind();
        }

        protected void btnSubmitInquiry_Click(object sender, EventArgs e)
        {
            try
            {
                TheHurlerEntities _ctx = new TheHurlerEntities();
                GetInTouch data = new GetInTouch
                {
                    Name = txtName.Text,
                    Email = txtEmail.Text,
                    ContactNumber = txtEmail.Text,
                    Message = txtMessage.Text,
                    subject = txtSubject.Text,
                };

                _ctx.GetInTouches.Add(data);
                _ctx.SaveChanges();

                txtEmail.Text = txtName.Text = txtEmail.Text = txtSubject.Text = txtMessage.Text = string.Empty;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", "InquirySubmittedSuccessfully()", true);
            }
            catch (Exception)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Failed", "FailedToSendInquiry()", true);
            }
        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }
    }
}