﻿namespace QuestSageProject.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual int BlogCount { get; set; }
    }
}