﻿namespace QuestSageProject.Models
{
    public class GetInTouchModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Budget { get; set; }
        public string Email { get; set; }
        public string subject { get; set; }
        public string Message { get; set; }        
    }
}