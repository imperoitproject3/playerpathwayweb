﻿using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;

namespace TheHurler
{
    public partial class Blog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int CategoryId = 0;
                if (Request.QueryString["CategoryId"] != null)
                {
                    CategoryId = Convert.ToInt32(Request.QueryString["CategoryId"]);
                }
                BindLateBlogs(CategoryId);
            }

        }
        private void BindLateBlogs(int CategoryId)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            List<BlogMasterModel> objBlogs = new List<BlogMasterModel>();

            if (CategoryId > 0)
            {
                objBlogs = (from b in _ctx.BlogMasters
                            where b.CategoryId == CategoryId
                            select new BlogMasterModel
                            {
                                BlogId = b.BlogId,
                                Title = b.Title,
                                Author = b.Author,
                                ImageUrl = b.ImageUrl,
                                PublishDate = b.PublishDate
                            }).OrderByDescending(x => x.PublishDate).ToList();
            }
            else
            {
                objBlogs = (from b in _ctx.BlogMasters
                            select new BlogMasterModel
                            {
                                BlogId = b.BlogId,
                                Title = b.Title,
                                Author = b.Author,
                                ImageUrl = b.ImageUrl,
                                PublishDate = b.PublishDate
                            }).OrderByDescending(x => x.PublishDate).ToList();
            }

            if (objBlogs != null && objBlogs.Any())
            {
                rptLatestBlog.DataSource = objBlogs;
                //ltlLatestBlogTitle.Text = objBlogs.First().Title;
            }
            else
                rptLatestBlog.DataSource = null;
            rptLatestBlog.DataBind();
        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }
    }
}