﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheHurler.Helper
{
    public static class Utilities
    {
        public static string StripHtml(string html)
        {
            var returnText = string.Empty;
            if (html == null || html == string.Empty)
                return string.Empty;
            var Text = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]*>", string.Empty);
            if (Text.Length > 0)
            {
                if (Text.Length > 100)
                {
                    returnText = Text.Substring(0, 100);
                }
                else
                {
                    returnText = Text;
                }
            }
            else
            {
                returnText = string.Empty;
            }
            return returnText;
        }
    }
}