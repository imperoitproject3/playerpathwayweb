﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clara-gaa-cul-camps-stats.aspx.cs" Inherits="TheHurler.clara_gaa_cul_camps_stats" %>

<!doctype html>
<html class="no-js" lang="zxx">


<!-- Mirrored from quomodosoft.com/html/appro/demo/blog-details-left-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Aug 2018 13:13:17 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Player Pathway Cúl Camp Freshford</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/apple-touch-icon.png") %>" />
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/favicon.ico") %>" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/bundles/Styles") %>" />
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' rel='stylesheet'>
     <script type="text/javascript" id="cookieinfo"
        src="//cookieinfoscript.com/js/cookieinfo.min.js"
        data-bg="#000000"
        data-fg="#FFFFFF"
        data-link="green"
           data-divlink="White"
        data-divlinkbg="green"
        data-cookie="CookieInfoScript"
        data-text-align="left"
        data-close-text="Agree"
        font-family="'Poppins', sans-serif"
        data-message="This website use cookies. By proceeding you agree to our "
        data-moreinfo="privacy_policy.aspx"
        data-linkmsg="Privacy Policy">
    </script>
    <style>
        .lrg-logo {
            height: 65px;
        }

        .sml-logo {
            height: 50px;
        }
    </style>
</head>

<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
    <div class="preloade">
        <span style="font-size: inherit !important"><i class="iconLoader"></i></span>
    </div>
    <!--Header-Area-->
    <header class="overlay-bg relative fix" id="home">
        <div class="section-bg blog-header"></div>
        <!--Mainmenu-->
        <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<%= Page.ResolveUrl("~/default.aspx") %>" class="navbar-brand">
                           <img class="lrg-logo" src="<%= Page.ResolveUrl("~/images/logo.png") %>" alt="Player pathway">
                    </a>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                    <ul class="nav navbar-nav">
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#home") %>">Home</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#about") %>">About</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#screens")%>">Screens</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/clara-gaa-cul-camps-stats.aspx") %>">Stats</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#blog") %>">Blog</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#contact")%>">Contact</a></li>
                             <li><a href="<%= Page.ResolveUrl("~/Privacy_policy.aspx") %>">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Mainmenu/-->
        <div class="space-50"></div>
        <!--Header-Text/-->
    </header>
    <!--Header-Area/-->
    <!--Blog-Section-->
    <section>
        <div class="space-80"></div>
        <div class="container">
            <div class="row">
                <asp:Repeater ID="rptRecentPost" runat="server">
                    <ItemTemplate>
                        <div class="col-xs-12 col-sm-12 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="panel text-center single-blog">
                                <div class="space-10"></div>
                                <div class="col-md-offset-2 col-xs-8 col-sm-8 padding-25">
                                    <a href="javascript:;">
                                        <h3><%# Eval("Title") %></h3>
                                    </a>
                                    <div class="space-5"></div>
                                    <p><%# Eval("Description") %></p>
                                    <div class="space-10"></div>
                                </div>
                                <img src="<%# Page.ResolveUrl("~/Files/GrphicImages/"+ Eval("ImageUrl")) %>" style="padding: 10px !important; height: 90%; width: 90%" class="img-full">
                            </div>
                            <div class="space-30"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <!--Blog-Section/-->
    <hr>
    <!--Footer-area-->
    <footer class="black-bg">
        <div class="container">
            <div class="space-80"></div>
            <div class="row text-white wow fadeIn">
                   <div class="row text-white wow fadeInUp">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <h2 class="text-uppercase text-center">Subscribe our newsletter</h2>
                        <div class="space-15"></div>
                      <form action="https://playerpathway.us19.list-manage.com/subscribe/post?u=7dea1cc9c8d39ec48ab7575ef&amp;id=eee8843c96" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscrie-form validate" target="_blank">
                            <label class="mt10" for="mc-email"></label>
                            <div class="input-group">
                                 <input type="email" name="EMAIL" class="form-control required" id="mce-EMAIL" placeholder="email address" required="required">
                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-info" value="Subscribe" name="subscribe" id="mc-embedded-subscribe">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="space-30"></div>
                 <div class="col-xs-12 text-center">
                        <div class="row wow fadeInUp">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
                                <p>Player Pathway was developed in partnership with Kilkenny GAA and supported by Leinster GAA, Croke Park, Kellogg’s GAA Cúl Camps, Turas, Enterprise Ireland and Institute of Technology Carlow.</p>
                            </div>
                        </div>
                        <div class="space-20"></div>
                        <div class="row">
                            <div class="col-xs-4 col-md-2 col-md-offset-1 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/1.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/2.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/3.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/4.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/5.png") %>" />
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="col-xs-12 text-center">
                    <div class="social-menu">
                        <a href="https://www.facebook.com/PlayerPathway/" target="_blank"><span class="ti-facebook"></span></a>
                        <a href="https://twitter.com/PlayerPathwayIE" target="_blank"><span class="ti-twitter-alt"></span></a>
                    </div>
                    <div class="space-20"></div>
                   <p>© 2018 <a target="_blank" href="https://squareroot.ie">Smart Stats Limited</a> - All rights reserved</p>
                </div>
            </div>
            <div class="space-20"></div>
        </div>
    </footer>
    <script src="<%= Page.ResolveUrl("~/bundles/jquery") %>"></script>
    <script type="text/javascript">
        $(function () {
            var logo = $(".lrg-logo"); $(window).scroll(function () {
                console.log();
                var scroll = $(window).scrollTop();

                if (scroll >= 100) {
                    if (!logo.hasClass("sml-logo")) {
                        logo.hide();
                        logo.removeClass('lrg-logo').addClass("sml-logo").fadeIn("slow");
                    }
                } else {
                    if (!logo.hasClass("lrg-logo")) {
                        logo.hide();
                        logo.removeClass("sml-logo").addClass('lrg-logo').fadeIn("slow");
                    }
                }
            });
        });
    </script>
</body>
</html>

