﻿using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;
using TheHurler.Models;

namespace TheHurler
{
    public partial class clara_gaa_cul_camps_stats : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindInfoGraphic();
            }
        }

        private void BindInfoGraphic()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            var objBlog = (from b in _ctx.InfoGraphics
                           select new InfographicModel
                           {
                               Id = b.Id,
                               Title = b.Title,
                               ImageUrl = b.ImageUrl,
                               Description = b.Description
                           }).OrderByDescending(x => x.Id).ToList();

            if (objBlog != null && objBlog.Any())
            {
                rptRecentPost.DataSource = objBlog;
            }
            else
            {
                //divRecentPost.Visible = false;
                rptRecentPost.DataSource = null;
            }
            rptRecentPost.DataBind();
        }
    }
}