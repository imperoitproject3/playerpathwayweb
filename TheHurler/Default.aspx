﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TheHurler.Default" %>

<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta property="og:title" content="Playerpathway" />
    <meta property="og:url" content="https://www.playerpathway.ie"/>
    <meta property="og:description" content="Playerpathway : Empowering Sports Performance">
    <title>Player Pathway</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/favicon.ico") %>" />
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/apple-touch-icon.png") %>" />
    <link rel="apple-touch-icon" sizes="57x57" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-57x57.png") %>">
    <link rel="apple-touch-icon" sizes="60x60" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-60x60.png") %>">
    <link rel="apple-touch-icon" sizes="72x72" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-72x72.png") %>">
    <link rel="apple-touch-icon" sizes="76x76" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-76x76.png") %>">
    <link rel="apple-touch-icon" sizes="114x114" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-114x114.png") %>">
    <link rel="apple-touch-icon" sizes="120x120" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-120x120.png") %>">
    <link rel="apple-touch-icon" sizes="144x144" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-144x144.png") %>">
    <link rel="apple-touch-icon" sizes="152x152" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-152x152.png") %>">
    <link rel="apple-touch-icon" sizes="180x180" href="<%= Page.ResolveUrl("~/images/Favicon/apple-icon-180x180.png") %>">
    <link rel="icon" type="image/png" sizes="192x192" href="<%= Page.ResolveUrl("~/images/Favicon/android-icon-192x192.png")%>">
    <link rel="icon" type="image/png" sizes="32x32" href="<%= Page.ResolveUrl("~/images/Favicon/favicon-32x32.png")%>">
    <link rel="icon" type="image/png" sizes="96x96" href="<%= Page.ResolveUrl("~/images/Favicon/favicon-96x96.png")%>">
    <link rel="icon" type="image/png" sizes="16x16" href="<%= Page.ResolveUrl("~/images/Favicon/favicon-16x16.png")%>">
    <link rel="manifest" href="<%= Page.ResolveUrl("~/images/Favicon/manifest.json")%>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<%= Page.ResolveUrl("~/images/Favicon/ms-icon-144x144.png")%>">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/bundles/Styles") %>" />
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' rel='stylesheet'>

    <script type="text/javascript" id="cookieinfo"
        src="//cookieinfoscript.com/js/cookieinfo.min.js"
        data-bg="#000000"
        data-fg="#FFFFFF"
        data-link="green"
        data-divlink="White"
        data-divlinkbg="green"
        data-cookie="CookieInfoScript"
        data-text-align="left"
        data-close-text="Agree"
        font-family="'Poppins', sans-serif"
        data-message="This website use cookies. By proceeding you agree to our "
        data-moreinfo="privacy_policy.aspx"
        data-linkmsg="Privacy Policy">
    </script>

    <style>
        h3 {
            color: green;
        }
    </style>
</head>
<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
    <div class="preloade">
        <span style="font-size: inherit !important"><i class="iconLoader" style="background-image: url('images/icons/loader.png')"></i></span>
    </div>
    <!--Header-Area-->
    <header class="blue-bg relative fix" id="home">
        <div class="section-bg overlay-bg dewo ripple"></div>
        <!--Mainmenu-->
        <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<%= Page.ResolveUrl("~/default.aspx") %>" class="navbar-brand">
                        <img class="lrg-logo" src="<%= Page.ResolveUrl("~/images/logo.png") %>" alt="Player pathway">
                    </a>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#screens">Screens</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/clara-gaa-cul-camps-stats.aspx") %>">Stats</a></li>
                        <li><a href="#blog">Blog</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/Privacy_policy.aspx") %>">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Mainmenu/-->
        <div class="space-100"></div>
        <div class="space-20 hidden-xs"></div>
        <!--Header-Text-->
        <div class="container text-white">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <div class="space-80"></div>
                    <h1>Empowering Sports Performance</h1>
                    <div class="space-10"></div>
                    <p class="appdescription">
                        <h4 align="justify">Empowering  coaches  to  optimise performance  through  the  aggregation  of  fundamentals  skills  and  education.
                            <br />
                            <br />
                            Player Pathway not only supports coaches but also allows individual athletes the opportunity to self-assess and provides them with a wealth of resources on nutrition and sport psychology.
                             <br />
                            <br />
                            Success in Sports is measured in fine margins, Player Pathway measures those margins.
                               <br />
                            <br />
                        </h4>
                        <div class="space-50"></div>
                        <a href="//playerpathway.ie/Files/Dwonload/player-pathway-portfolio.pdf" target="_blank" class="btn btn-icon"><span class="ti-agenda"></span>
                            <h4>Read Case Study</h4>
                        </a>
                    </p>

                </div>
                <div class="hidden-xs hidden-sm col-md-4">
                    <div class="home_screen_slide">
                        <div class="single_screen_slide wow fadeInRight">
                            <div class="item">
                                <img src="images/screen/screen1.jpg" alt="Playerpathway-Homw">
                            </div>
                            <div class="item">
                                <img src="images/screen/screen2.jpg" alt="Playerpathway-Access-level">
                            </div>
                            <div class="item">
                                <img src="images/screen/screen3.jpg" alt="Playerpathway-club">
                            </div>
                            <div class="item">
                                <img src="images/screen/screen4.jpg" alt="Playerpathway-ballyhale">
                            </div>
                            <div class="item">
                                <img src="images/screen/screen5.jpg" alt="Playerpathway-skill-challenges">
                            </div>
                        </div>
                    </div>
                    <div class="home_screen_nav">
                        <span class="ti-angle-left testi_prev"></span>
                        <span class="ti-angle-right testi_next"></span>
                    </div>
                </div>
            </div>
            <div class="space-80"></div>
        </div>
        <!--Header-Text/-->
    </header>
    <section class="gray-bg" id="about">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-md-offset-1 col-xs-12 col-md-10 text-center">
                    <h3 class="text-uppercase">About Player Pathway</h3>
                    <br />

                    <div class="col-xs-12 col-md-12 text-center">
                        <p>
                            Although millions of Euro is invested in player development across sport each year, there remains no effective means of accurately evaluating athletes ability and progress from a young age.
                            <br />
                            The answer to the above problem: Empower coaches with the best tools available. 
                            <br />
                            Player pathway is just that tool. Providing a set of fundamental skills, categorised by an athletes age or skill level, allow coaches the ability to quantify their player's abilities, identify their weaknesses & structure their coaching sessions based on results.
                            <br />
                            Each coach has access to the wealth of statistical data based on their assessments, even comparing stats from previous teams and regional averages in extraordinary detail. 
                            <br />
                            <b>Success in Sports is measured in fine margins, Player Pathway measures those margins.</b>
                        </p>
                    </div>

                </div>
                <div class="space-60"></div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 text-center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="border hover-shadow">
                            <div class="space-50">
                                <img class="height50 img-fluid" src="images/icons/howitworks.png" alt="">
                            </div>
                            <div class="space-30"></div>
                            <h3 class="text-uppercase">What is Player pathway?</h3>
                            <div class="space-20"></div>
                            <p>
                                A  platform  for  coaches  at  all  levels  of    sports  to  professionally  monitor  and  evaluate  players  progression  across  all  stages  of  development.<br />
                                <br />
                                Portable  and  pre-populated  software  built  with  ease  of  use  to  enable  all  users  the  ability  to  maximise  its  value  to  the  player/coach/club/organisation.
                                <br />
                                <br />
                                Players/clubs  assessments  can  be  easily  quantified  allowing  clubs  and  organisations  the  option  to  easily  measure  player’s  abilities  and  development  and  adjust  their  training  approaches  in  the  interest  of  the  players. 
                                 <br />
                                <br />

                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 text-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="border hover-shadow">
                            <div class="space-50">
                                <img class="height50 img-fluid" src="images/icons/about.png" alt="playerpathway-about">
                            </div>
                            <div class="space-30"></div>
                            <h3 class="text-uppercase">How It Works</h3>
                            <div class="space-20"></div>
                            <p>
                                Each  club  receive  a  unique  login  that  can  be  made  available  to  all  coaching  members. 
                                <br />
                                <br />
                                Clubs  will  have  the  opportunity  to  assess  their  players  within  2-3  time  periods  per  year  where  upon  completion  they  will  receive  a  detailed  statistical  analysis  of  their  players’  performances. 
                                <br />
                                <br />
                                These  performances  will  have  comparisons  with  previous  assessments  and  county  averages.
                                <br />
                                <br />

                            </p>
                        </div>
                    </div>
                </div>
                <div class="space-60"></div>
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-md-offset-2 text-center wow fadeInUp">
                        <div class="down-offset ">
                            <img class="img-thumbnail" src="images/Award.png" alt="playerpathway-award">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="screens" class="fix">
        <div class="space-180"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">APP SCREENSHOTS</h3>
                </div>
                <div class="space-20"></div>
                <div class="row text-white wow fadeInUp">
                    <div class="col-xs-12 col-sm-3 col-sm-offset-3 text-center">
                        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.thehurler&hl=en" class="big-button">
                            <span class="big-button-icon">
                                <span class="ti-android"></span>
                            </span>
                            <span>Available on</span>
                            <br>
                            <strong>Play store</strong>
                        </a>
                        <div class="space-10"></div>
                    </div>
                    <div class="col-xs-12 col-sm-3 text-center">
                        <a target="_blank" href="https://itunes.apple.com/gb/app/gaa-player-development-the-hurler/id1241636178?mt=8" class="big-button">
                            <span class="big-button-icon">
                                <span class="ti-apple"></span>
                            </span>
                            <span>Available on</span>
                            <br>
                            <strong>iTunes</strong>
                        </a>
                        <div class="space-10"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="space-40"></div>
        <div class="row wow fadeIn">
            <div class="col-xs-12">
                <div class="space-60"></div>
                <div class="list_screen_slide">
                    <div class="item">
                        <a href="images/screen/screen1.jpg" class="work-popup">
                            <img src="images/screen/screen1.jpg" alt="Playerpathway-Home">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen2.jpg" class="work-popup">
                            <img src="images/screen/screen2.jpg" alt="Playerpathway-Access-level">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen3.jpg" class="work-popup">
                            <img src="images/screen/screen3.jpg" alt="Playerpathway-club">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen4.jpg" class="work-popup">
                            <img src="images/screen/screen4.jpg" alt="Playerpathway-ballyhale">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen5.jpg" class="work-popup">
                            <img src="images/screen/screen5.jpg" alt="Playerpathway-challenges">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen6.jpg" class="work-popup">
                            <img src="images/screen/screen6.jpg" alt="Playerpathway">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen7.jpg" class="work-popup">
                            <img src="images/screen/screen7.jpg" alt="Playerpathway">
                        </a>
                    </div>
                    <div class="item">
                        <a href="images/screen/screen8.jpg" class="work-popup">
                            <img src="images/screen/screen8.jpg" alt="Playerpathway">
                        </a>
                    </div>
                </div>
                <div class="space-40"></div>
            </div>
        </div>
        <div class="space-80"></div>
    </section>
    <section class="gray-bg" id="1">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">Social Media</h3>
                    <p>Please follow us on Social media</p>
                </div>
            </div>
            <div class="space-20"></div>
            <div class="row text-white wow fadeInUp">
                <div class="col-md-offset-4 col-sm-offset-2 col-sm-8 col-md-4 text-center">
                    <div class="col-xs-6 col-sm-6 text-center">
                        <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FPlayerPathway%2F&width=63&layout=button&action=like&size=large&show_faces=false&share=false&height=65&appId" width="63" height="65" style="border: none; overflow: hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                    </div>
                    <div class="col-xs-6 col-sm-6 text-center">
                        <a class="twitter-follow-button"
                            href="https://twitter.com/PlayerPathwayIE"
                            data-show-screen-name="false"
                            data-show-count="false"
                            data-size="large">Follow @TwitterDev</a>
                    </div>
                </div>
            </div>
            <div class="space-20"></div>
            <div class="row wow fadeInUp">
                <div class="row">
                    <div class="col-md-offset-3 col-xs-12 col-sm-12 col-md-6 text-center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="border hover-shadow">
                            <a class="twitter-timeline" data-height="500" data-link-color="#008000" href="https://twitter.com/PlayerPathwayIE?ref_src=twsrc%5Etfw">Tweets by PlayerPathwayIE</a>
                            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="space-80"></div>
    </section>
    <!--Download-Section/-->
    <!--Blog-Section-->
    <section id="blog">
        <div class="space-80"></div>
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                    <h3 class="text-uppercase">LATEST FROM BLOG</h3>
                </div>
            </div>
            <div class="space-60"></div>
            <div class="row">
                <asp:Repeater ID="rptBlogs" runat="server">
                    <ItemTemplate>
                        <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="panel text-center single-blog">
                                <img style="height: 224px;" class="img-full" src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" alt="<%# Eval("Title") %>">
                                <div class="padding-20">
                                    <ul class="list-unstyled list-inline">
                                        <li><span class="ti-user"></span>By : <%# Eval("Author") %></li>
                                        <li><span class="ti-calendar"></span><%# string.Format("{0: MMM dd, yyyy}", Eval("PublishDate")) %></li>
                                    </ul>
                                    <div class="space-10"></div>
                                    <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">
                                        <h3><%# Eval("Title") %></h3>
                                    </a>
                                    <div class="space-15"></div>
                                    <p><%# Eval("BlogBody") %></p>
                                    <div class="space-20"></div>
                                    <a class="btn btn-link" href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">Read more <i class="fas fa-caret-right"></i></a>
                                    <div class="space-20"></div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="row">
                <div class="space-60"></div>
                <div class="col-xs-12 text-center">
                    <a href="<%= Page.ResolveUrl("~/blog.aspx")%>" class="btn btn-link active">View All</a>
                </div>
                <div class="space-60"></div>
            </div>
        </div>
    </section>
    <div id="contact">
        <div class="space-180"></div>
        <footer class="black-bg">
            <div class="container">
                <div class="row">
                    <div class="offset-top">
                        <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-lg">
                                <h3>Get in Touch</h3>
                                <div class="space-20"></div>
                                <form runat="server" method="post">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="form-name" class="sr-only">Name</label>
                                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name*"></asp:TextBox>
                                                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>

                                            </div>
                                            <div class="space-10"></div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="form-email" class="sr-only">Email</label>
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="E-mail*"></asp:TextBox>
                                                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator Style="color: red;" ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Please enter valid email" ValidationGroup="Inquiry" Display="Dynamic"></asp:RegularExpressionValidator>
                                            </div>
                                            <div class="space-10"></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="form-subject" class="sr-only">Subject</label>
                                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" placeholder="Subject*"></asp:TextBox>
                                                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtSubject" ErrorMessage="Please enter subject" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="space-10"></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label for="form-message" class="sr-only">comment</label>
                                                <asp:TextBox ID="txtMessage" Rows="6" TextMode="MultiLine" runat="server" CssClass="form-control" placeholder="Message"></asp:TextBox>
                                            </div>
                                            <div class="space-10"></div>
                                            <asp:Button ID="btnSubmitInquiry" runat="server" CssClass="btn btn-link no-round text-uppercase" Text="Send message" OnClick="btnSubmitInquiry_Click" ValidationGroup="Inquiry" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="well well-lg">
                                <h3>About Us</h3>
                                <div class="space-20"></div>
                                <p align="justify">
                                    Player Pathway was developed in response to the millions of Euro invested in player development across sport each year that had no effective means of accurately evaluating a player’s ability and progress. It solves this problem by empowering coaches with a tool that can effectively measure sports performance.<br />
                                    "Success in Sport is Measured in Fine Margins, Player Pathway Measures those Margins”.
                                </p>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="border-icon sm"><span class="ti-headphone"></span></div>
                                            </td>
                                            <td><a href="callto:+353874147412 ">+35 3874 147 412</a></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="border-icon sm"><span class="ti-email"></span></div>
                                            </td>
                                            <td>
                                                <a href="mailto: info@playerpathway.ie">info@playerpathway.ie</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="border-icon sm"><span class="ti-location-pin"></span></div>
                                            </td>
                                            <td>
                                                <address><a href="javascript:;">Castlecomer, Co.Kilkenny, Ireland</a></address>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="space-25"></div>
                <div class="row text-white wow fadeIn">
                    <div class="row text-white wow fadeInUp">
                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                            <h2 class="text-uppercase text-center">subscribe our newsletter</h2>
                            <div class="space-10"></div>
                            <form action="https://playerpathway.us19.list-manage.com/subscribe/post?u=7dea1cc9c8d39ec48ab7575ef&amp;id=eee8843c96" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscrie-form validate" target="_blank">
                                <label class="mt10" for="mc-email"></label>
                                <div class="input-group">
                                    <input type="email" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="email address" required="required">
                                    <div class="input-group-btn">
                                        <input type="submit" class="btn btn-info" value="Subscribe" name="subscribe" id="mc-embedded-subscribe">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="space-25"></div>
                    <div class="col-xs-12 text-center">
                        <div class="row wow fadeInUp">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
                                <p>Player Pathway was developed in partnership with Kilkenny GAA and supported by Leinster GAA, Croke Park, Kellogg’s GAA Cúl Camps, Turas, Enterprise Ireland and Institute of Technology Carlow.</p>
                            </div>
                        </div>
                        <div class="space-20"></div>
                        <div class="row">
                            <div class="col-xs-4 col-md-2 col-md-offset-1 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/1.png") %>" alt="turas" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/2.png") %>" alt="carlow" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/3.png") %>" alt="cul" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/4.png") %>" alt="leinscer" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/5.png") %>" alt="crokepark" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <div class="social-menu">
                            <a href="https://www.facebook.com/PlayerPathway/" target="_blank"><span class="ti-facebook"></span></a>
                            <a href="https://twitter.com/PlayerPathwayIE" target="_blank"><span class="ti-twitter-alt"></span></a>
                        </div>
                        <div class="space-20"></div>
                        <p>© 2018 <a target="_blank" href="https://squareroot.ie">Smart Stats Limited</a> - All rights reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="<%= Page.ResolveUrl("~/bundles/jquery") %>"></script>
</body>

</html>
