﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BlogDetail.aspx.cs" Inherits="TheHurler.BlogDetail" %>

<!doctype html>
<html class="no-js" lang="zxx">
<!-- Mirrored from quomodosoft.com/html/appro/demo/blog-details-left-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 28 Aug 2018 13:13:17 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Player Pathway Blog Details</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/apple-touch-icon.png") %>" />
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/favicon.ico") %>" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/bundles/Styles") %>" />
    <link href='https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' rel='stylesheet'>
    <script type="text/javascript" id="cookieinfo"
        src="//cookieinfoscript.com/js/cookieinfo.min.js"
        data-bg="#000000"
        data-fg="#FFFFFF"
        data-link="green"
        data-divlink="White"
        data-divlinkbg="green"
        data-cookie="CookieInfoScript"
        data-text-align="left"
        data-close-text="Agree"
        font-family="'Poppins', sans-serif"
        data-message="This website use cookies. By proceeding you agree to our "
        data-moreinfo="privacy_policy.aspx"
        data-linkmsg="Privacy Policy">
    </script>
</head>

<body data-spy="scroll" data-target="#mainmenu" data-offset="50">
    <div class="preloade">
        <span style="font-size: inherit !important"><i class="iconLoader"></i></span>
    </div>
    <!--Header-Area-->
    <header class="overlay-bg relative fix" id="home">
        <div class="section-bg blog-header"></div>
        <!--Mainmenu-->
        <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="<%= Page.ResolveUrl("~/default.aspx") %>" class="navbar-brand">
                        <img class="lrg-logo" src="<%= Page.ResolveUrl("~/images/logo.png") %>" alt="Player pathway">
                    </a>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                    <ul class="nav navbar-nav">
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#home") %>">Home</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#about") %>">About</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#screens")%>">Screens</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/clara-gaa-cul-camps-stats.aspx") %>">Stats</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#blog") %>">Blog</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#contact")%>">Contact</a></li>
                             <li><a href="<%= Page.ResolveUrl("~/Privacy_policy.aspx") %>">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Mainmenu/-->
        <div class="space-100"></div>
        <div class="container text-white">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="text-uppercase">Blog Details</h2>
                </div>
            </div>
            <div class="space-100"></div>
        </div>
        <!--Header-Text/-->
    </header>
    <!--Header-Area/-->
    <!--Blog-Section-->
    <section>
        <div class="space-80"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-9 pull-right">
                    <div class="blog-details">
                        <asp:Image ID="blogImage" runat="server" />
                        <div class="space-15"></div>
                        <ul class="breadcrumb">
                            <li><a href="#"><span class="ti-user"></span>By  
                                <asp:Literal ID="ltlPostedBy" runat="server"></asp:Literal></a></li>
                            <li><a href="#"><span class="ti-calendar"></span>
                                <asp:Literal ID="ltlPostedOn" runat="server"></asp:Literal></a></li>
                        </ul>
                        <div class="space-20"></div>
                        <h2>
                            <asp:Literal ID="ltlBlogTitle" runat="server"></asp:Literal></h2>
                        <div class="space-20"></div>
                        <p id="divBlogBody" runat="server"></p>
                        <div class="space-40"></div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <ul class="list-inline list-unstyled social-menu text-center pull-left">
                                </ul>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="space-5"></div>
                                <ul class="list-inline list-unstyled text-right">
                                    <li><strong>Tags: </strong></li>
                                    <li>
                                        <asp:Literal ID="ltlTagName" runat="server"></asp:Literal>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="space-80"></div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <!--Sidebar-->
                    <aside class="sidebar">
                        <div class="space-60"></div>
                        <div class="single-sidebar category">
                            <h3>Category</h3>
                            <hr>
                            <ul class="list-unstyled">
                                <asp:Repeater ID="rptCategories" runat="server">
                                    <ItemTemplate>
                                        <li data-animate="fadeInUp" data-delay=".3" class="category<%# Eval("Id") %>">
                                            <a href="<%# Page.ResolveUrl("blog.aspx?categoryid="+Eval("Id")) %>"><%# Eval("Name") %> <span>(<%# Eval("BlogCount") %>)</span></a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <div class="space-60"></div>
                        <!--Sidebar-popular-post-->
                        <div class="single-sidebar" id="divRecentPost" runat="server">
                            <h3>Recent post</h3>
                            <hr>
                            <ul class="list-unstyled">
                                <asp:Repeater ID="rptRecentPost" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">
                                                <span class="alignleft">
                                                    <img src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" height="62px" width="62px">
                                                </span>
                                                <h5><%# Eval("Title") %></h5>
                                                <small><%# string.Format("{0:MMM dd, yyyy}", Eval("PublishDate")) %></small>
                                            </a>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <!--Sidebar-popular-post-->
                        <div class="space-60"></div>
                    </aside>
                    <!--Sidebar/-->
                </div>
            </div>
        </div>
        <div class="space-80"></div>
    </section>
    <!--Blog-Section/-->
    <hr>
    <!--Footer-area-->
    <footer class="black-bg">
        <div class="container">
            <div class="space-80"></div>
            <div class="row text-white wow fadeIn">
                <div class="row text-white wow fadeInUp">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <h2 class="text-uppercase text-center">Subscribe our newsletter</h2>
                        <div class="space-15"></div>
                        <form action="https://playerpathway.us19.list-manage.com/subscribe/post?u=7dea1cc9c8d39ec48ab7575ef&amp;id=eee8843c96" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscrie-form validate" target="_blank">
                            <label class="mt10" for="mc-email"></label>
                            <div class="input-group">
                                <input type="email" name="EMAIL" class="form-control required" id="mce-EMAIL" placeholder="email address" required="required">
                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-info" value="Subscribe" name="subscribe" id="mc-embedded-subscribe">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="space-30"></div>
                <div class="col-xs-12 text-center">
                    <div class="row wow fadeInUp">
                        <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
                            <p>Player Pathway was developed in partnership with Kilkenny GAA and supported by Leinster GAA, Croke Park, Kellogg’s GAA Cúl Camps, Turas, Enterprise Ireland and Institute of Technology Carlow.</p>
                        </div>
                    </div>
                    <div class="space-20"></div>
                    <div class="row">
                        <div class="col-xs-4 col-md-2 col-md-offset-1 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-hover text-center">
                                <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/1.png") %>" />

                            </div>
                        </div>
                        <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-hover text-center">
                                <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/2.png") %>" />

                            </div>
                        </div>
                        <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-hover text-center">
                                <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/3.png") %>" />

                            </div>
                        </div>
                        <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-hover text-center">
                                <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/4.png") %>" />

                            </div>
                        </div>
                        <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="well well-hover text-center">
                                <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/5.png") %>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <div class="social-menu">
                        <a href="https://www.facebook.com/PlayerPathway/" target="_blank"><span class="ti-facebook"></span></a>
                        <a href="https://twitter.com/PlayerPathwayIE" target="_blank"><span class="ti-twitter-alt"></span></a>
                    </div>
                    <div class="space-20"></div>
                    <p>© 2018 <a target="_blank" href="https://squareroot.ie">Smart Stats Limited</a> - All rights reserved</p>
                </div>
            </div>
            <div class="space-20"></div>
        </div>
    </footer>
    <script src="<%= Page.ResolveUrl("~/bundles/jquery") %>"></script>
</body>
</html>
