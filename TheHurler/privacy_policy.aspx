﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="privacy_policy.aspx.cs" Inherits="TheHurler.privacy_policy" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Player Pathway Privacy Policy</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/apple-touch-icon.png") %>" />
    <link rel="shortcut icon" type="image/ico" href="<%= Page.ResolveUrl("~/images/favicon.ico") %>" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/bundles/Styles") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" id="cookieinfo"
        src="//cookieinfoscript.com/js/cookieinfo.min.js"
        data-bg="#000000"
        data-fg="#FFFFFF"
        data-link="green"
        data-divlink="White"
        data-divlinkbg="green"
        data-cookie="CookieInfoScript"
        data-text-align="left"
        data-close-text="Agree"
        font-family="'Poppins', sans-serif"
        data-message="This website use cookies. By proceeding you agree to our "
        data-moreinfo="privacy_policy.aspx"
        data-linkmsg="Privacy Policy">
    </script>
    <script>
        $(function () {
            $("#accordion").accordion({
                heightStyle: "content",
                collapsible: true
            });
        });
    </script>
    <style>
        .ui-state-active, .ui-state-disabled:hover .ui-button:hover, .ui-button:focus {
            border: 1px solid Green;
            background: green;
            color: white
        }

        .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus, .ui-button:hover, .ui-button:focus {
            border: 1px solid Green;
            background: green;
            color: white
        }

        h3 {
            font-size: 18px !important;
        }

        [class^="ti-"], [class*=" ti-"] {
            line-height: inherit;
        }

        .ui-accordion .ui-accordion-header {
            margin: 10px 0 0 0;
        }
    </style>
</head>
<body>
    <header class="overlay-bg relative fix" id="home">
        <!--Mainmenu-->
        <nav class="navbar navbar-default mainmenu-area navbar-fixed-top" data-spy="affix" data-offset-top="60">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" class="navbar-toggle" data-target="#mainmenu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a href="<%= Page.ResolveUrl("~/default.aspx") %>" class="navbar-brand">
                        <img class="lrg-logo" src="<%= Page.ResolveUrl("~/images/logo.png") %>" alt="Player pathway">
                    </a>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="mainmenu">
                    <ul class="nav navbar-nav">
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#home") %>">Home</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#about") %>">About</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#screens")%>">Screens</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/clara-gaa-cul-camps-stats.aspx") %>">Stats</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#blog") %>">Blog</a></li>
                        <li><a href="<%= Page.ResolveUrl("~/default.aspx#contact")%>">Contact</a></li>
                             <li><a href="<%= Page.ResolveUrl("~/Privacy_policy.aspx") %>">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--Mainmenu/-->
    </header>
    <section>

        <div class="space-120"></div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <h1>Privacy And Cookie Policy</h1>
                    <h4>Effective as of 23rd May 2018</h4>
                    <div class="space-15"></div>
                    <div id="accordion" style="font-family: 'Poppins', sans-serif !important;">
                        <h3>1. Introduction</h3>
                        <div>
                            <p>
                                We fully respect your right to privacy and to protection of your personal data. Any personal information which you provide to us will be treated strictly in accordance with the Data Protection Acts 1988 and 2003 and once it is introduced on May 25, 2017 GDPR. We ask that you read this privacy and cookie statement ('Statement') carefully as it contains important information as to how we use your personal data and our cookie policy.
                            </p>
                        </div>
                        <h3>2. Who we are?</h3>
                        <div>
                            <p>
                                Any references in this policy to “we”, “us”, “our”, is to “Player Pathway”. Player Pathway, is a division of Smart Stats Limited registered at Kiltown, Castlecomer, Co. Kilkenny, Ireland. 
                            </p>
                        </div>
                        <h3>3. Your Data Protection rights?</h3>
                        <div>
                            <p>
                                You have a number of rights which you may exercise in respect of your personal data. If you wish to exercise any of these rights, please email info@playerpathway.ie detailing the right that you wish to exercise together with official documentation confirming your identity such as passport, driving licence. We must respond to you without delay and in any event within one month (subject to limited extensions). You are entitled to lodge a complaint with the Data Protection Commissioner if you are not happy with our response when you chose to exercise any of your rights below. The following are a synopsis of your data protection rights. We are allowed to refuse your request in certain limited circumstances. If this arises we will let you know.
                            </p>
                            <ul>
                                <li><b>Right to information</b> -You have a right to certain information in relation to your personal data. We have included this in this document.</li>
                                <li><b>Right of access</b> - You have the right to receive information from us in relation to the personal data that we hold about you.</li>
                                <li><b>Right of erasure/right to be forgotten</b> -  This is your right to have information which we hold in relation to you deleted in certain circumstances.</li>
                                <li><b>Right to rectification</b> - This is your right to have inaccurate personal data held by us concerning you corrected and incomplete personal data completed.</li>
                                <li><b>Right to object</b> - It is your right to object to the processing of your personal data by us but only where we are processing the personal data on the grounds of legitimate interest.</li>
                                <li><b>Right to restriction</b> - It is your right to require us to limit the processing of your personal data where certain circumstances arise.</li>
                                <li><b>Right to data portability </b>-This is a right to have your personal information transferred in an electronic and structured form to you or to another party This enables you to take your data from us in an electronically useable format and to be able to transfer your data to another party in an electronically useable format.</li>
                                <li><b>Right to object to automated decision making including profiling</b> - This is a right not to be the subject of automated decision-making by us using your personal information or profiling of you.</li>
                                <li><b>Right to withdraw consent</b> - where we are processing your personal data on the grounds of consent you have a right to withdraw your consent to the processing.<br />
                                    The Company at all times reserves the right to process personal data for the establishment, exercise or defence of legal claims.
                                </li>
                            </ul>
                        </div>
                        <h3>4. Security of your personal data</h3>
                        <div>
                            <p>
                                Any personal information which Player Pathway collects about you will be treated with the highest standards of security and confidentiality, strictly in accordance with the Data Protection Acts, 1988 & 2003 and when it is introduced on 25 May 2018 GDPR.<br />
                                We take our responsibilities to protect your personal data very seriously and employ the most appropriate physical measures including staff training and awareness and we review these measures regularly.<br />
                                We have undertaken a security review of any third parties described with whom we share your personal data.
                            </p>
                            <p>
                            </p>
                        </div>
                        <h3>5. Clients</h3>
                        <div>
                            <p>
                                We collect the data that you actively provide during the following activities:<br>
                                <br>
                                when filling in forms on our website (www.PlayerPathway.ie) and other social media; during phone calls; in other correspondence e.g. email.<br>
                                in other correspondence e.g. email.<br>
                                <br>
                                The following data can be collected actively from you:-<br>
                                <br>
                                First and last name;<br>
                                The organisation for which you work;<br>
                                E-mail address;<br>
                                Job Title.;<br>
                                Phone number (where contact is made by phone);<br>
                                VAT number.<br>
                                <br>
                                As well as the information that you actively provide to us if you visit our site to browse, read or download information from our website, certain statistical information is available to us via our internet service provider. This information may include:<br>
                                <br>
                                the IP address from which you access our website;<br>
                                the top-level domain name used (for example .ie, .com, .org, .net);<br>
                                the type of browser and operating system used to access our site;<br>
                                the date and time of your access to our site;<br>
                                the pages you visit;<br>
                                and any previous website address from which you reached us, including any search terms used.<br>
                                <br>
                                If you fill out a form on our website providing us with your personal data as outlined above, this may enable us to identify you on future visits to the website by combining the personal data that you have provided to us in the form and your IP address. This will enable us to identify your particular preferences and to particularly market those products and services to you. If you are already a client of the Company, we may chat to you about these particular products and services. Unless you provide us with your personal data through the form we will not be able to identify you on future visits to the website by your IP address although we may be able to identify more generally the company with which you work.<br>
                                <br>
                                We also use cookies to facilitate the use of our website. For more information on the cookies we use and the purposes for which we use them, see the section of our policy relating to cookies below.<br>
                                <br>
                                In addition, we also collect certain statistical information when you read our newsletter.
                            </p>
                            <h4>5.2 Use of your personal data – why and for how long</h4>
                            <p>
                                We will only process any personal data which you provide to us in accordance with the purpose for which it was provided<br>
                                <br>
                                If you are our client we will use your personal data:<br>
                                <br>
                                to provide products and services to you in accordance with any agreements entered into between us. For this purposes, we will store your personal data in our Client Relationship Management System (“CRM”); in accordance with the legitimate interests of the Company to generate as many sales as possible and to promote our organisation as a leader in its field we may send you information by e-mail from time to time about our products and services that might be of interest to you, security updates, white papers and events. You will always be provided with the opportunity to opt- out of receiving these communications; in an anonymised format for research, statistical analysis and behavioural analysis; and for diagnosing problems with the website and fraud prevention and detection.
                                <br>
                                <br>
                                Due to the longevity of the life of many of the products and consultancy services that we provide, unless notified otherwise by you we retain your personal data for five years from the date that you purchase products or services from us and delete the personal data after this period has expired.
                                <br>
                                <br>
                                If you are not a client of the Company but have provided us with consent to contact you with information in relation to our products and services, events, whitepapers, security updates or any other information we will only send you the information that you have asked us to send to you. You can withdraw your consent to our sending you this information at any time. We will continue to send you this information unless you tell us that you no longer require it. We will send emails reminding you of your right to withdraw your consent from time to time.
                            </p>
                            <h4>5.3 Who do we share your personal data with</h4>


                            We do not sell or distribute your personal information to third parties for purposes of allowing them to market products and services to you.<br>
                            <br>
                            <strong>Marketing: </strong>We use Mailchimp for sending digital forms and direct marketing emails and managing mailing lists. Mailchimp stores the personal data in the United States for the purpose of providing us with this service. Mailchimp have self-certified that they adhere to the EU-US Privacy Shield framework. Please see a link to Mailchimps’s Privacy Statement https://mailchimp.com/legal/privacy/. We have entered into an agreement with Mailchimp to ensure that they will only use your data for the purpose of our agreement with them and on our instructions.<br>
                            <br>
                            <strong>Sub-contractors:</strong>  From time to time we may use sub-contractors in the provision of the services to you – all sub-contractors use Player Pathway equipment in providing the services to you and have entered agreements with Player Pathway to ensure that they only use your data for the purpose of our agreement with them and on our instructions.<br>
                            <br>
                            <strong>CRM:</strong>  We are currently migrating to a new client relationship management system which will ensure that we provide you with the best possible service. We will ensure that the CRM system used will only use your data for the purpose of our agreement with them and on our instructions.<br>
                            <br>
                            <strong>By law:</strong>We may also have to disclose your personal data if we are required to do so by law.
                            <br>
                            <br>
                            <strong>Sale or other transfer:</strong> We may also disclose your personal data in connection with any merger, sale of company assets, or acquisition of all or a portion of our business by another company. If any of these events were to happen, this policy would continue to apply to your information and the party receiving your information may continue to use your information, but only consistent with this privacy Statement.<br>
                            <br>
                            <strong>Aggregated and De-Identified Information:</strong> We may share information that has been aggregated or reasonably de-identified, so that the information could not reasonably be used to identify you. For instance, we may publish aggregate statistics about the use of our Services.
                        </div>

                        <h3>6. Suppliers/Partners</h3>
                        <div>
                            <p>
                                We collect the following information from you in respect of the individual in an organization from whom we order products and/or services/pay for products and/or services.<br>
                                First and last name;<br>
                                The organisation for which you work;<br>
                                E-mail address;<br>
                                Job Title.;<br>
                                Phone number (where contact is made by phone or through contact form);<br>
                                VAT number.<br>
                                Bank account details.<br>
                            </p>
                            <h4>6.2. Use of your personal data – why and for how long</h4>
                            <p>
                                We will only use your personal data for the purpose of our rights and obligations under any agreement entered into between us and in particular to order and pay for products/services and request you to carry out your obligations under the agreement.<br>
                                <br>
                                We will hold your personal data for the duration of the engagement/agreement between us. We will delete the information once the agreement between us terminates and it is not to be renewed. 
                            </p>
                            <h4>6.3. Use of your personal data – why and for how long</h4>
                            <p>
                                We do not share your data with other parties.
                            </p>
                            <p>
                            </p>
                        </div>

                        <h3>7.Cookies Policy</h3>
                        <div>
                            <p>
                                Cookies are small pieces of information, stored in simple text files, placed on your computer or internet enabled device by a web site. Typically, it is used to ease navigation through the Website. Cookies can be read by the web site on your subsequent visits. In addition, the information stored in a cookie may relate to your browsing habits on the web page, or a unique identification number so that the web site can remember you on your return visit. By using our website you are agreeing to our use of cookies.<br>
                                <br>
                                We may use cookies for the following purposes:<br>
                                <br>
                                <strong>Analytics and Tags </strong>– We use cookies to learn how you, and other users, use our services and how well they perform. This allows us to identify errors, learn how our services perform, and improve and develop our services over time. We use Google Analytics and Google Tags: This is a web analytics service provided by Google Inc. which uses cookies to show us how visitors found and explored our site, and how we can enhance their experience. It provides us with information about the behaviour of our visitors (e.g. how long they stayed on the site, the average number of pages viewed) and also tells us how many visitors we have had. The aggregated statistical data cover items such as total visits or page views, and referrers to our website. For further details on Google’s privacy policies see <a target="_blank" href='https://www.google.com/analytics/learn/privacy.html'>https://www.google.com/analytics/learn/privacy.html</a>.<br>
                                <br>
                                <strong>Website Optimisation</strong> – We use cookies to improve the operation and presentation of our website. Our website is built using common internet platforms. These have built-in cookies which help compatibility issues (e.g., to identify your browser type) and improve performance (e.g., quicker loading of content).<br>
                                <br>
                                <strong>Marketing</strong> – Through Mailchimp, we use pixel tags to track and improve the effectiveness of our marketing emails. For example, we may use such technologies to identify if you read a marketing or update we sent to you.<br>
                                <br>
                                <strong>Functionality</strong> – These cookies remember choices you made to improve your experience on our website.<br>
                                <br>
                                <strong>Targeting or advertising cookies</strong> – These cookies collect information about your browsing habits on the website in order to make advertising relevant to you and your interests.<br>
                                <br>
                                <strong>Wordpress</strong> – the Website is hosted on Wordpress. Please see link to their cookie policy attached - <a target="_blank" href='https://en.support.wordpress.com/cookies'>https://en.support.wordpress.com/cookies</a>
                                <br>
                                <br>
                                Youtube –the videos on our website are hosted through Youtube. Please see attached link to the Youtube privacy policy - <a target="_blank" href='https://www.youtube.com/static?template=privacy_guidelines'>https://www.youtube.com/static?template=privacy_guidelines</a>.
                            </p>
                            <p>
                            </p>
                        </div>

                        <h3>8. Changes to Policy</h3>
                        <div>
                            <p>
                                We may change this Policy from time to time. We will notify you of any changes to the Policy.
                            </p>
                            <p>
                            </p>
                        </div>

                        <h3>9. Governing Law and Jurisdiction</h3>
                        <div>
                            <p>
                                This Policy and all issues regarding our website are governed by the laws of Ireland and are subject to the exclusive jurisdiction of the courts of Ireland
                            </p>
                            <p>
                            </p>
                        </div>

                        <h3>10.  Contact details</h3>
                        <div>
                            <p>
                                We welcome your feedback and questions. If you wish to contact us, please send an email to info@playerpathway.ie or you can write to us at, Kiltown, Castlecomer, Co. Kilkenny, Ireland.
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <!--Footer-area-->
    <footer class="black-bg">
        <div class="container">
            <div class="space-80"></div>
            <div class="row text-white wow fadeIn">
                <div class="row text-white wow fadeInUp">
                    <div class="col-xs-12 col-md-6 col-md-offset-3">
                        <h2 class="text-uppercase text-center">Subscribe our newsletter</h2>
                        <div class="space-15"></div>
                      <form action="https://playerpathway.us19.list-manage.com/subscribe/post?u=7dea1cc9c8d39ec48ab7575ef&amp;id=eee8843c96" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="subscrie-form validate" target="_blank">
                            <label class="mt10" for="mc-email"></label>
                            <div class="input-group">
                                 <input type="email" name="EMAIL" class="form-control required" id="mce-EMAIL" placeholder="email address" required="required">
                                <div class="input-group-btn">
                                    <input type="submit" class="btn btn-info" value="Subscribe" name="subscribe" id="mc-embedded-subscribe">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="space-30"></div>
               <div class="col-xs-12 text-center">
                        <div class="row wow fadeInUp">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
                                <p>Player Pathway was developed in partnership with Kilkenny GAA and supported by Leinster GAA, Croke Park, Kellogg’s GAA Cúl Camps, Turas, Enterprise Ireland and Institute of Technology Carlow.</p>
                            </div>
                        </div>
                        <div class="space-20"></div>
                        <div class="row">
                            <div class="col-xs-4 col-md-2 col-md-offset-1 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/1.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/2.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/3.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/4.png") %>" />

                                </div>
                            </div>
                            <div class="col-xs-4 col-md-2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="well well-hover text-center">
                                    <img class="img-fluid" src="<%= Page.ResolveUrl("~/images/logo/5.png") %>" />
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-xs-12 text-center">
                    <div class="social-menu">
                        <a href="https://www.facebook.com/PlayerPathway/" target="_blank"><span class="ti-facebook"></span></a>
                        <a href="https://twitter.com/PlayerPathwayIE" target="_blank"><span class="ti-twitter-alt"></span></a>
                    </div>
                    <div class="space-20"></div>
                    <p>© 2018 <a target="_blank" href="https://squareroot.ie">Smart Stats Limited</a> - All rights reserved</p>
                </div>
            </div>
            <div class="space-20"></div>
        </div>
    </footer>
</body>
</html>