﻿using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;
using TheHurler.Models;

namespace TheHurler.Admin
{
    public partial class Infographic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }

        }

        private void BindGrid()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            List<InfographicModel> infographics = (from b in _ctx.InfoGraphics
                                           select new InfographicModel
                                           {
                                               Id = b.Id,
                                               Title = b.Title,
                                               Description = b.Description,
                                           }).ToList();

            GV.DataSource = infographics;
            GV.DataBind();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int Id = Convert.ToInt32(e.CommandArgument);
                if (Id > 0)
                {
                    TheHurlerEntities _ctx = new TheHurlerEntities();
                    var objDelete = _ctx.InfoGraphics.FirstOrDefault(x => x.Id == Id);
                    if (objDelete != null)
                    {
                        _ctx.InfoGraphics.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindGrid();
                    }
                }
            }
        }
    }
}