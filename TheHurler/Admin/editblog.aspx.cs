﻿using TheHurler.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using QuestSageProject.Models;

namespace TheHurler.Admin
{
    public partial class editblog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["BlogId"] != null)
                {
                    BindCategoryDropdown();
                    int BlogId = Convert.ToInt32(Request.QueryString["BlogId"].ToString());
                    BindBlog(BlogId);
                }
                else
                    Response.Redirect("blogs.aspx");
            }
        }

        private void BindCategoryDropdown()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            List<CategoryModel> categories = (from c in _ctx.Categories
                                              select new CategoryModel
                                              {
                                                  Id = c.Id,
                                                  Name = c.Name
                                              }).ToList();

            ddlCategory.DataSource = categories;
            ddlCategory.DataBind();
        }

        private void BindBlog(int BlogId)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            BlogMaster data = _ctx.BlogMasters.FirstOrDefault(x => x.BlogId == BlogId);
            if (data == null)
            {
                Response.Redirect("blogs.aspx");
            }
            else
            {
                txtTitle.Text = data.Title;
                txtAuthor.Text = data.Author;
                txtDescription.Text = data.BlogBody;
                txtTag.Text = data.Tag;
                imgBlog.ImageUrl = ResolveUrl("/Files/BlogImages/" + data.ImageUrl);
                ddlCategory.SelectedValue = data.CategoryId.ToString();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int BlogId = Convert.ToInt32(Request.QueryString["BlogId"].ToString());
            TheHurlerEntities _ctx = new TheHurlerEntities();

            if (_ctx.BlogMasters.Any(x => x.Title.ToLower() == txtTitle.Text.ToString().ToLower() && x.BlogId != BlogId))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateBlog", "fnDuplicateBlog()", true);
            }
            else
            {
                BlogMaster data = _ctx.BlogMasters.FirstOrDefault(x => x.BlogId == BlogId);

                if (data != null)
                {
                    if (fileImage != null && fileImage.HasFile)
                    {
                        string ogFileName = fileImage.FileName.Trim('\"');
                        string shortGuid = Guid.NewGuid().ToString();
                        var thisFileName = BlogId + "_" + shortGuid + Path.GetExtension(ogFileName);

                        string BlogImagesPath = Server.MapPath("~/Files/BlogImages/");
                        fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                        data.ImageUrl = thisFileName;
                    }

                    data.CategoryId = Convert.ToInt32(ddlCategory.SelectedValue);
                    data.Title = txtTitle.Text;
                    data.Author = txtAuthor.Text;
                    data.BlogBody = txtDescription.Text;
                    data.Tag = txtTag.Text;
                    _ctx.SaveChanges();
                }
                Response.Redirect("blogs.aspx");
            }
        }
    }
}