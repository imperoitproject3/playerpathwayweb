﻿using TheHurler.DAL;
using System;
using System.Linq;

namespace TheHurler.Admin
{
    public partial class addcategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            Category data = _ctx.Categories.FirstOrDefault(x => x.Name.ToLower() == txtName.Text.ToString().ToLower());
            if (data != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateCategory", "fnDuplicateCategory()", true);
            }
            else
            {
                Category objCategory = new Category
                {
                    Name = txtName.Text
                };
                _ctx.Categories.Add(objCategory);
                _ctx.SaveChanges();
                Response.Redirect("categories.aspx");
            }            
        }
    }
}