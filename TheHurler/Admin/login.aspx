﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="TheHurler.Admin.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Bootstrap/CSS/bootstrap.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Bootstrap/CSS/bootstrap-extended.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/CSS/Core/app.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/CSS/Core/colors.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/CSS/Fonts/icomoon.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/pace/pace.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/animate/animate.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Toaster/toastr.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Toaster/toastr.min.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/CSS/Custom/custom.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/iCheck/icheck-blue.css") %>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/Pages/login-register/login-register.css") %>" rel="stylesheet" />
</head>

<body data-open="click" data-menu="vertical-content-menu" data-col="1-column" class="vertical-layout vertical-content-menu 1-column bg-cyan bg-lighten-2 blank-page blank-page  pace-done">
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <form id="form1" runat="server">
                    <%--@RenderBody()--%>
                    <section class="flexbox-container">
                        <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header no-border">
                                    <div class="card-title text-xs-center">
                                        <div class="p-1">
                                            <h4>Admin Login</h4>
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                                        <span>Login with Questsage</span>
                                    </h6>
                                </div>                                
                                <div class="card-body collapse in">
                                    <div class="col-lg-12"></div>
                                    <div class="clearfix"></div>
                                    <div class="card-block">
                                        <fieldset class="form-group position-relative has-icon-left mb-0">
                                            <asp:TextBox ID="emailId" runat="server" CssClass="form-control form-control-lg input-lg" placeholder="Enter Username"></asp:TextBox>
                                            <div class="form-control-position">
                                                <i class="icon-head"></i>
                                            </div>
                                        </fieldset>
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <asp:TextBox ID="password" runat="server" TextMode="Password" CssClass="form-control form-control-lg input-lg" placeholder="Enter Password"></asp:TextBox>
                                            <div class="form-control-position">
                                                <i class="icon-key3"></i>
                                            </div>
                                        </fieldset>
                                        <asp:Label ID="lblError" runat="server" style="color:red;"></asp:Label>
                                        <asp:Button ID="btnSubmit" runat="server" Text="Login" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnSubmit_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>

    <script src="<%= Page.ResolveUrl("~/Content/Jquery/main/jquery.min.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/Content/Jquery/ui/tether.min.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/Content/Plugins/Bootstrap/Jquery/bootstrap.min.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/Content/Plugins/pace/pace.min.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/Content/Jquery/scripts.js") %>"></script>
</body>
</html>