﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;

namespace TheHurler.Admin
{
    public partial class editinfoGraphic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Id"] != null)
                {
                    int infoGraphicId = Convert.ToInt32(Request.QueryString["Id"].ToString());
                    BindInfoGraphic(infoGraphicId);
                }
                else
                    Response.Redirect("infoGraphic.aspx");
            }
        }

        private void BindInfoGraphic(int Id)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            var data = _ctx.InfoGraphics.FirstOrDefault(x => x.Id == Id);
            if (data == null)
            {
                Response.Redirect("infoGraphic.aspx");
            }
            else
            {
                txtTitle.Text = data.Title;
                txtDescription.Text = data.Description;
                imgBlog.ImageUrl = ResolveUrl("/Files/GrphicImages/" + data.ImageUrl);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int Id = Convert.ToInt32(Request.QueryString["Id"].ToString());
            TheHurlerEntities _ctx = new TheHurlerEntities();

            var data = _ctx.InfoGraphics.FirstOrDefault(x => x.Id == Id);
            if (data != null)
            {
                if (fileImage != null && fileImage.HasFile)
                {
                    string ogFileName = fileImage.FileName.Trim('\"');
                    string shortGuid = Guid.NewGuid().ToString();
                    var thisFileName = Id + "_" + shortGuid + Path.GetExtension(ogFileName);

                    string BlogImagesPath = Server.MapPath("~/Files/GrphicImages/");
                    fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                    data.ImageUrl = thisFileName;
                }

                data.Title = txtTitle.Text;
                data.Description = txtDescription.Text;
                _ctx.SaveChanges();
            }
            Response.Redirect("infoGraphic.aspx");
        }
    }
}