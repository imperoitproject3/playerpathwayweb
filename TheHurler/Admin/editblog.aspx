﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/TheHurlerAdmin.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="editblog.aspx.cs" Inherits="TheHurler.Admin.editblog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Summernote/summernote.css") %>" rel="stylesheet" />

    <script src="<%= Page.ResolveUrl("~/Content/Plugins/Summernote/summernote.min.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            $('#liBlogs').addClass('active');
            $('#txtDescription').summernote({
                height: 250,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false,               
            });
        });

        function fnDuplicateBlog() {
            alert('Another blog already exists with same title!');
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>--%>
    <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add New Blog</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Category <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Category"></span></label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlCategory" runat="server" DataTextField="Name" DataValueField="Id" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="ddlCategory" ErrorMessage="Please select category"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Title <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Title"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtTitle" ErrorMessage="Please enter title"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Author <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Author"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtAuthor" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAuthor" ErrorMessage="Please enter author name" Style="color: red;"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Tag <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Tag"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtTag" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtTag" ErrorMessage="Please enter tag"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Uploaded Image <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title=""></span></label>
                                        <div class="col-md-4">
                                            <asp:Image ID="imgBlog" runat="server" Height="200" Width="300" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Image <span class="red">(770w X 350h)</span><span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Image"></span></label>
                                        <div class="col-md-4">
                                            <asp:FileUpload ID="fileImage" runat="server" CssClass="form-control" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Description <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Description"></span></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtDescription" ClientIDMode="Static" runat="server" CssClass="form-control" TextMode="MultiLine" />
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtDescription" ErrorMessage="Please enter description"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("blogs.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
