﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;

namespace TheHurler.Admin
{
    public partial class addinfoGraphic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            if (fileImage != null && fileImage.HasFile)
            {
                InfoGraphic objGraphic = new InfoGraphic
                {
                    Title = txtTitle.Text.ToString(),
                    Description = txtDescription.Text,
                    ImageUrl = "A",
                };

                string ogFileName = fileImage.FileName.Trim('\"');
                string shortGuid = Guid.NewGuid().ToString();
                var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                string BlogImagesPath = Server.MapPath("~/Files/GrphicImages/");
                fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                objGraphic.ImageUrl = thisFileName;
                _ctx.InfoGraphics.Add(objGraphic);
                _ctx.SaveChanges();
            }
            Response.Redirect("InfoGraphic.aspx");
        }
    }
}