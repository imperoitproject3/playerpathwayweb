﻿using TheHurler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuestSageProject.Models;

namespace TheHurler.Admin
{
    public partial class categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            List<CategoryModel> blogs = (from c in _ctx.Categories
                                         select new CategoryModel
                                         {
                                             Id = c.Id,
                                             Name = c.Name
                                         }).ToList();

            GV.DataSource = blogs;
            GV.DataBind();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int Category = Convert.ToInt32(e.CommandArgument);
                if (Category > 0)
                {
                    TheHurlerEntities _ctx = new TheHurlerEntities();
                    Category objDelete = _ctx.Categories.FirstOrDefault(x => x.Id == Category);
                    if (objDelete != null)
                    {
                        _ctx.Categories.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindGrid();
                    }
                }
            }
        }
    }
}