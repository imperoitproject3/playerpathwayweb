﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/TheHurlerAdmin.Master" AutoEventWireup="true" CodeBehind="addinfoGraphic.aspx.cs" Inherits="TheHurler.Admin.addinfoGraphic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        $(function () {
            $('#liInfoGraphic').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add New Info graphic</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Title <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Title"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtTitle" ErrorMessage="Please enter title"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Image <span class="red"></span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Author"></span></label>
                                        <div class="col-md-4">
                                            <asp:FileUpload ID="fileImage" runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="fileImage" ErrorMessage="Please upload image"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Description <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Description"></span></label>
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtDescription" ClientIDMode="Static" runat="server" CssClass="form-control" TextMode="MultiLine" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("infographic.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
