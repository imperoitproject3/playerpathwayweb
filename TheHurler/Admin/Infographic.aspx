﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/TheHurlerAdmin.Master" AutoEventWireup="true" CodeBehind="Infographic.aspx.cs" Inherits="TheHurler.Admin.Infographic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        $(function () {
            $('#liInfoGraphic').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="basic">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">All Info Graphics</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a href="<%= Page.ResolveUrl("addinfoGraphic.aspx") %>">
                                        <i class="icon-plus3"></i>Add Info graphic
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div id="basicScenario" class="jsgrid" style="position: relative; height: auto; width: 100%;">
                                <div class="jsgrid-grid-body">
                                    <table class="Datatable table table-hover table-striped" style="margin-top: 0px;">
                                        <thead style="display: table-header-group;">
                                            <tr>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Title</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner ">Description</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                 <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner "></div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater runat="server" ID="GV" OnItemCommand="GV_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Title")%></td>
                                                        <td><%# Eval("Description")%></td>
                                                        <td>
                                                            <a class="btn btn-sm btn-info" href="editinfoGraphic.aspx?Id=<%# Eval("Id")%>">Edit
                                                            </a>                                                            
                                                            <asp:LinkButton runat="server" class="btn btn-sm btn-danger m_top_5" CommandArgument='<%# Eval("Id")%>' CommandName="delete"
                                                                OnClientClick="return ConfirmOnDelete();">Delete
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
