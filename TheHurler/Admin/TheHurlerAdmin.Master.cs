﻿using System;

namespace TheHurler.Admin
{
    public partial class TheHurlerAdmin : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminID"] == null)
            {
                Response.Redirect("login.aspx");
            }
        }
    }
}