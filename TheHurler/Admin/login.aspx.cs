﻿using TheHurler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TheHurler.Admin
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();
            DAL.Login objLogin = _ctx.Logins.FirstOrDefault(x => x.UserName == emailId.Text && x.Password == password.Text);
            if (objLogin != null)
            {
                Session["AdminID"] = objLogin.AdminID;
                Response.Redirect("blogs.aspx");
            }
            else
            {
                lblError.Text = "Invalid username or password";
            }
        }
    }
}