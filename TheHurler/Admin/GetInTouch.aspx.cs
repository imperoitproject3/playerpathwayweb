﻿using TheHurler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuestSageProject.Models;

namespace TheHurler.Admin
{
    public partial class GetInTouch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindMessages();
            }
        }

        private void BindMessages()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            List<GetInTouchModel> lstGetInTouch = (from i in _ctx.GetInTouches
                                                   select new GetInTouchModel
                                                   {
                                                       Id = i.Id,
                                                       Name = i.Name,
                                                       ContactNumber = i.ContactNumber,
                                                       Budget = i.Budget,
                                                       Email = i.Email,
                                                       subject = i.subject,
                                                       Message = i.Message
                                                   }).ToList();

            if (lstGetInTouch != null && lstGetInTouch.Any())
                rptGetInTouch.DataSource = lstGetInTouch;
            else
                rptGetInTouch.DataSource = null;
            rptGetInTouch.DataBind();
        }

        protected void rptGetInTouch_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int Id = Convert.ToInt32(e.CommandArgument);
                if (Id > 0)
                {
                    TheHurlerEntities _ctx = new TheHurlerEntities();
                    DAL.GetInTouch objDelete = _ctx.GetInTouches.FirstOrDefault(x => x.Id == Id);
                    if (objDelete != null)
                    {
                        _ctx.GetInTouches.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindMessages();
                    }
                }
            }
        }
    }
}