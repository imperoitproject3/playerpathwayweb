﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;
using System.Web;

namespace TheHurler.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/Styles").Include(
               "~/css/bootstrap.min.css",
               "~/css/owl.carousel.min.css",
               "~/css/themify-icons.css",
               "~/css/animate.css",
               "~/css/magnific-popup.css",
               "~/css/space.css",
               "~/css/theme.css",
               "~/css/overright.css",
               "~/css/normalize.css",
               "~/css/style.css",
               "~/css/responsive.css"
                ));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/js/vendor/jquery-1.12.4.min.js",
                "~/js/vendor/modernizr-2.8.3.min.js",
                "~/js/vendor/bootstrap.min.js",
                "~/js/owl.carousel.min.js",
                "~/js/scrollUp.min.js",
                "~/js/magnific-popup.min.js",
                "~/js/ripples-min.js",
                "~/js/contact-form.js",
                "~/js/spectragram.min.js",
                "~/js/ajaxchimp.js",
                "~/js/wow.min.js",
                "~/js/plugins.js",
                "~/js/main.js",
                "~/js/Custom.js"
            ));


            BundleTable.EnableOptimizations = false;
        }
    }
}