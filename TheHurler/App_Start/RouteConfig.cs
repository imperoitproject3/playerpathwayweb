using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace TheHurler
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;

            routes.MapPageRoute("Blog",
                "blogdetail/{title}/{id}",
                "~/BlogDetail.aspx");

            routes.EnableFriendlyUrls(settings);
        }
    }
}
