﻿using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TheHurler.DAL;

namespace TheHurler
{
    public partial class BlogDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.RequestContext.RouteData.Values.ContainsKey("title") && Request.RequestContext.RouteData.Values.ContainsKey("id"))
            {
                string title = Request.RequestContext.RouteData.Values["title"].ToString();
                int BlogId = Convert.ToInt32(Request.RequestContext.RouteData.Values["id"]);

                BindBlogDetail(BlogId);
                BindRecentBlogs(BlogId);
                BindCategories();
            }
            else
                Response.Redirect("~/blog.aspx");
        }
        private void BindRecentBlogs(int BlogId)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            List<BlogMasterModel> objBlog = (from b in _ctx.BlogMasters
                                             where b.BlogId != BlogId
                                             select new BlogMasterModel
                                             {
                                                 BlogId = b.BlogId,
                                                 Title = b.Title,
                                                 PublishDate = b.PublishDate,
                                                 ImageUrl = b.ImageUrl
                                             }).OrderByDescending(x => x.PublishDate).Take(5).ToList();

            if (objBlog != null && objBlog.Any())
            {
                rptRecentPost.DataSource = objBlog;
            }
            else
            {
                divRecentPost.Visible = false;
                rptRecentPost.DataSource = null;
            }
            rptRecentPost.DataBind();
        }

        private void BindBlogDetail(int BlogId)
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            BlogMasterModel model = (from b in _ctx.BlogMasters
                                     where b.BlogId==BlogId
                                     select new BlogMasterModel
                                     {
                                         CategoryId = b.CategoryId,
                                         BlogId = b.BlogId,
                                         Title = b.Title,
                                         Author = b.Author,
                                         CategoryName = b.Category.Name,
                                         Tag = b.Tag,
                                         PublishDate = b.PublishDate,
                                         BlogBody = b.BlogBody,
                                         ImageUrl = b.ImageUrl
                                     }).FirstOrDefault();

            if (model == null)
                Response.Redirect("~/Blog.aspx");

            ltlPostedOn.Text = model.PublishDate.ToString("MMMM dd, yyyy");
            ltlBlogTitle.Text = model.Title;
            ltlPostedBy.Text = model.Author;
            ltlTagName.Text = model.Tag;
            blogImage.ImageUrl = Page.ResolveUrl("~/Files/BlogImages/" + model.ImageUrl);
            divBlogBody.InnerHtml = model.BlogBody;
        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        private void BindCategories()
        {
            TheHurlerEntities _ctx = new TheHurlerEntities();

            List<CategoryModel> lstCategories = (from c in _ctx.Categories
                                                 select new CategoryModel
                                                 {
                                                     Id = c.Id,
                                                     Name = c.Name,
                                                     BlogCount = c.BlogMasters.Count()
                                                 }).Where(x => x.BlogCount > 0).ToList();

            rptCategories.DataSource = lstCategories;
            rptCategories.DataBind();
        }
    }
}